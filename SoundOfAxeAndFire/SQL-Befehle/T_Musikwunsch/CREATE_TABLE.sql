CREATE TABLE IF NOT EXISTS `T_Musikwunsch`(
	`titelName` 	VARCHAR(100) 	NOT NULL,
	`bandName` 		VARCHAR(100) 	NOT NULL,
	`genre` 		VARCHAR(50) 	NOT NULL,
	PRIMARY KEY (`titelName`, `bandName`)
);