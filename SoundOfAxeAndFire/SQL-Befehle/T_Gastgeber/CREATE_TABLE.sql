CREATE TABLE IF NOT EXISTS `T_Gastgeber`(
	`name` 		VARCHAR(50) 	NOT NULL,
	CONSTRAINT pk_Gastgeber 
	PRIMARY KEY (`name`),
	
	CONSTRAINT fk_Gastgeber 
	FOREIGN KEY (`name`) 
	REFERENCES T_Gast (`name`)
	
	ON DELETE CASCADE
	ON UPDATE CASCADE
);