CREATE TABLE IF NOT EXISTS `T_Gast_Musikwunsch`(
	`name` 			VARCHAR(50) 	NOT NULL,
	`titelName` 	VARCHAR(100) 	NOT NULL,
	`bandName` 		VARCHAR(100) 	NOT NULL,
	`voteNr`		INT 			NOT NULL,
	CONSTRAINT pk_Gast_Musikwunsch 
		PRIMARY KEY (`name`, `titelName`, `bandName`, `voteNr`),
	CONSTRAINT fk_Gast 
		FOREIGN KEY (`name`) 
		REFERENCES `T_Gast`(`name`)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT fk_Musikwunsch 
		FOREIGN KEY (`titelName`, `bandName`) 
		REFERENCES `T_Musikwunsch`(`titelName`, `bandName`)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);