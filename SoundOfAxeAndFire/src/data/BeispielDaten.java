package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

// Klasse, die dafür da ist Beispieldaten zu laden
public class BeispielDaten {
	
	// Verbindungsinformationen
	private static String		driver			= "com.mysql.jdbc.Driver";
	private static String		url				= "jdbc:mysql://localhost/SoundOfAxeAndFire?";
	private static String		user			= "root";
	private static String		password		= "";
	
	// Array mit Testdaten für Gäste
	private static String[]		gaeste			= {"Arslan", "Christian", "Christoph", "Danny", "David", "Dennis B.", "Dennis S.", "Dennis P.", "Domenik", "Emil", "Erik", "Finn", "Friedrich", "Gökhan", "Hussein", "Jakob", "Janik", "Jeldrik", "Jörg", "Julian", "Justin", "Kenneth", "Kolja", "Lars", "Lea", "Luc", "Luca", "Lukas", "Maximilian P.", "Maximilian S.", "Nikolas", "Pascal", "Pavel", "Philipp", "Saarujan", "Temuujin", "Tim W.", "Tom"};
	
	// Array mit Testdaten für Musikwünsche
	private static String[][]	musikwunsch		= {{"Back in Black", "AC/DC", "Heavy Metal"}, {"Highway To Hell", "AC/DC", "Heavy Metal"}, {"Hells Bells", "AC/DC", "Heavy Metal"}, {"Let Me Put My Love Into You", "AC/DC", "Heavy Metal"}, {"You Shook Me All Night Long", "AC/DC", "Heavy Metal"}, {"Down With the Sickness", "Disturbed", "Alternative-Metal"}, {"Decadence", "Disturbed", "Alternative-Metal"}, {"Down With the Sickness", "Disturbed", "Alternative-Metal"}, {"Violence Fetish", "Disturbed", "Alternative-Metal"}, {"Eiszeit", "Eisbrecher", "NDH"}, {"Die Engel", "Eisbrecher", "NDH"}, {"Segne Deinen Schmerz", "Eisbrecher", "NDH"}, {"Amok", "Eisbrecher", "NDH"}, {"Supermodel", "Eisbrecher", "NDH"}, {"Schwarze Witwe", "Eisbrecher", "NDH"}, {"Heilig", "Eisbrecher", "NDH"}, {"This Is Deutsch", "Eisbrecher", "NDH"}, {"Hail And Kill", "Manowar", "True Metal"}, {"Warriors of the World", "Manowar", "True Metal"}, {"Enter Sandman", "Metallica", "Thrash Metal"}, {"Wherever I May Roam", "Metallica", "Thrash Metal"}, {"Nothing Else Matters", "Metallica", "Thrash Metal"}, {"Of Wolf and Man", "Metallica", "Thrash Metal"}, {"Turn the Page", "Metallica", "Thrash Metal"}, {"All Nightmare Long", "Metallica", "Thrash Metal"}, {"Sabbra Cadabra", "Metallica", "Thrash Metal"}, {"Die, Die My Darling", "Metallica", "Thrash Metal"}, {"Whiskey in the Jar", "Metallica", "Thrash Metal"}, {"Seek & Destroy", "Metallica", "Thrash Metal"}, {"Enter Sandman", "Metallica", "Thrash Metal"}, {"Master of Puppets", "Metallica", "Thrash Metal"}, {"Enter Sandman", "Metallica", "Thrash Metal"}, {"Crazy Train", "Ozzy Osbourne", "Heavy Metal"}, {"Bark at the Moon", "Ozzy Osbourne", "Heavy Metal"}, {"Fire in the Sky", "Ozzy Osbourne", "Heavy Metal"}, {"No More Tears", "Ozzy Osbourne", "Heavy Metal"}, {"Dreamer", "Ozzy Osbourne", "Heavy Metal"}, {"Killers with the Cross", "Powerwolf", "Power-Metal"}, {"Army of the Night", "Powerwolf", "Power-Metal"}, {"Sanctified with Dynamite", "Powerwolf", "Power-Metal"}, {"Lust for Blood", "Powerwolf", "Power-Metal"}, {"Deutschland", "Rammstein", "NDH"}, {"Mein Herz Brennt", "Rammstein", "NDH"}, {"Rammlied", "Rammstein", "NDH"}, {"Du Riechst So Gut", "Rammstein", "NDH"}, {"Sonne", "Rammstein", "NDH"}, {"Waidmanns Heil", "Rammstein", "NDH"}, {"Laichzeit", "Rammstein", "NDH"}, {"Zwitter", "Rammstein", "NDH"}, {"Feuer Frei", "Rammstein", "NDH"}, {"Mann Gegen Mann", "Rammstein", "NDH"}, {"Engel", "Rammstein", "NDH"}, {"Keine Lust", "Rammstein", "NDH"}, {"Du Hast", "Rammstein", "NDH"}, {"Ich Will", "Rammstein", "NDH"}, {"Mein Land", "Rammstein", "NDH"}, {"Kokain", "Rammstein", "NDH"}, {"Reise, Reise", "Rammstein", "NDH"}, {"Chronic Slaughter", "Slaughter to Prevail", "Death-Core"}, {"Over The Wall", "Testament", "Thrash Metal"}, {"The New Order", "Testament", "Thrash Metal"}};
	
	// Array mit Testdaten für die Votes
	private static String[][]	gastMusikwunsch	= {{"Arslan", "Back in Black", "AC/DC", "1"}, {"Arslan", "Segne Deinen Schmerz", "Eisbrecher", "2"}, {"Arslan", "Mein Herz Brennt", "Rammstein", "3"}, {"Arslan", "Deutschland", "Rammstein", "4"}, {"Arslan", "Sabbra Cadabra", "Metallica", "5"}, {"Christian", "Crazy Train", "Ozzy Osbourne", "1"}, {"Christian", "Down With the Sickness", "Disturbed", "2"}, {"Christian", "Sonne", "Rammstein", "3"}, {"Christian", "Kokain", "Rammstein", "4"}, {"Christian", "Waidmanns Heil", "Rammstein", "5"}, {"Christoph", "Turn the Page", "Metallica", "1"}, {"Christoph", "Eiszeit", "Eisbrecher", "2"}, {"Christoph", "Eiszeit", "Eisbrecher", "3"}, {"Christoph", "Sonne", "Rammstein", "4"}, {"Christoph", "Sonne", "Rammstein", "5"}, {"Danny", "Mein Herz Brennt", "Rammstein", "1"}, {"Danny", "Rammlied", "Rammstein", "2"}, {"Danny", "Sonne", "Rammstein", "3"}, {"Danny", "Waidmanns Heil", "Rammstein", "4"}, {"Danny", "Deutschland", "Rammstein", "5"}, {"David", "Crazy Train", "Ozzy Osbourne", "1"}, {"David", "Crazy Train", "Ozzy Osbourne", "2"}, {"David", "Wherever I May Roam", "Metallica", "3"}, {"David", "Wherever I May Roam", "Metallica", "4"}, {"David", "Wherever I May Roam", "Metallica", "5"}, {"Dennis B.", "Army of the Night", "Powerwolf", "1"}, {"Dennis B.", "Army of the Night", "Powerwolf", "2"}, {"Dennis B.", "Sanctified with Dynamite", "Powerwolf", "3"}, {"Dennis B.", "Whiskey in the Jar", "Metallica", "4"}, {"Dennis B.", "Du riechst so gut", "Rammstein", "5"}, {"Dennis P.", "Of Wolf and Man", "Metallica", "1"}, {"Dennis P.", "Of Wolf and Man", "Metallica", "2"}, {"Dennis P.", "Of Wolf and Man", "Metallica", "3"}, {"Dennis P.", "Of Wolf and Man", "Metallica", "4"}, {"Dennis P.", "Of Wolf and Man", "Metallica", "5"}, {"Dennis S.", "Deutschland", "Rammstein", "1"}, {"Dennis S.", "Deutschland", "Rammstein", "2"}, {"Dennis S.", "Chronic Slaughter", "Slaughter to Prevail", "3"}, {"Dennis S.", "Chronic Slaughter", "Slaughter to Prevail", "4"}, {"Dennis S.", "Chronic Slaughter", "Slaughter to Prevail", "5"}, {"Domenik", "Die Engel", "Eisbrecher", "1"}, {"Domenik", "Reise, Reise", "Rammstein", "2"}, {"Domenik", "Reise, Reise", "Rammstein", "3"}, {"Domenik", "Reise, Reise", "Rammstein", "4"}, {"Domenik", "Reise, Reise", "Rammstein", "5"}, {"Emil", "Dreamer", "Ozzy Osbourne", "1"}, {"Emil", "Dreamer", "Ozzy Osbourne", "2"}, {"Emil", "Dreamer", "Ozzy Osbourne", "3"}, {"Emil", "Dreamer", "Ozzy Osbourne", "4"}, {"Emil", "Dreamer", "Ozzy Osbourne", "5"}, {"Erik", "Enter Sandman", "Metallica", "1"}, {"Erik", "Enter Sandman", "Metallica", "2"}, {"Erik", "Enter Sandman", "Metallica", "3"}, {"Erik", "Seek & Destroy", "Metallica", "4"}, {"Erik", "Supermodel", "Eisbrecher", "5"}, {"Finn", "The New Order", "Testament", "1"}, {"Finn", "Supermodel", "Eisbrecher", "2"}, {"Finn", "The New Order", "Testament", "3"}, {"Finn", "Deutschland", "Rammstein", "4"}, {"Finn", "Supermodel", "Eisbrecher", "5"}, {"Friedrich", "Back in Black", "AC/DC", "1"}, {"Friedrich", "Back in Black", "AC/DC", "2"}, {"Friedrich", "Highway To Hell", "AC/DC", "3"}, {"Friedrich", "Highway To Hell", "AC/DC", "4"}, {"Friedrich", "Highway To Hell", "AC/DC", "5"}, {"Gökhan", "Du Riechst So Gut", "Rammstein", "1"}, {"Gökhan", "Du Riechst So Gut", "Rammstein", "2"}, {"Gökhan", "Mann Gegen Mann", "Rammstein", "3"}, {"Gökhan", "Du Riechst So Gut", "Rammstein", "4"}, {"Gökhan", "Sonne", "Rammstein", "5"}, {"Hussein", "Violence Fetish", "Disturbed", "1"}, {"Hussein", "Violence Fetish", "Disturbed", "2"}, {"Hussein", "Decadence", "Disturbed", "3"}, {"Hussein", "Decadence", "Disturbed", "4"}, {"Hussein", "Down With the Sickness", "Disturbed", "5"}, {"Jakob", "Deutschland", "Rammstein", "1"}, {"Jakob", "Deutschland", "Rammstein", "2"}, {"Jakob", "Deutschland", "Rammstein", "3"}, {"Jakob", "Deutschland", "Rammstein", "4"}, {"Jakob", "Deutschland", "Rammstein", "5"}, {"Janik", "All Nightmare Long", "Metallica", "1"}, {"Janik", "Sabbra Cadabra", "Metallica", "2"}, {"Janik", "Supermodel", "Eisbrecher", "3"}, {"Janik", "Supermodel", "Eisbrecher", "4"}, {"Janik", "Eiszeit", "Eisbrecher", "5"}, {"Jeldrik", "Nothing Else Matters", "Metallica", "1"}, {"Jeldrik", "Sonne", "Rammstein", "2"}, {"Jeldrik", "Nothing Else Matters", "Metallica", "3"}, {"Jeldrik", "Sonne", "Rammstein", "4"}, {"Jeldrik", "Nothing Else Matters", "Metallica", "5"}, {"Jörg", "Army of the Night", "Powerwolf", "1"}, {"Jörg", "Army of the Night", "Powerwolf", "2"}, {"Jörg", "Army of the Night", "Powerwolf", "3"}, {"Jörg", "Enter Sandman", "Metallica", "4"}, {"Jörg", "Enter Sandman", "Metallica", "5"}, {"Julian", "Fire in the Sky", "Ozzy Osbourne", "1"}, {"Julian", "Sonne", "Rammstein", "2"}, {"Julian", "Deutschland", "Rammstein", "3"}, {"Julian", "Die Engel", "Eisbrecher", "4"}, {"Julian", "Heilig", "Eisbrecher", "5"}, {"Justin", "Highway To Hell", "AC/DC", "1"}, {"Justin", "Engel", "Rammstein", "2"}, {"Justin", "Eiszeit", "Eisbrecher", "3"}, {"Justin", "Lust for Blood", "Powerwolf", "4"}, {"Justin", "Army of the Night", "Powerwolf", "5"}, {"Kenneth", "Sanctified with Dynamite", "Powerwolf", "1"}, {"Kenneth", "Sanctified with Dynamite ", "Powerwolf", "2"}, {"Kenneth", "Lust for Blood", "Powerwolf", "3"}, {"Kenneth", "Army of the Night", "Powerwolf", "4"}, {"Kenneth", "Army of the Night", "Powerwolf", "5"}, {"Kolja", "Turn the Page", "Metallica", "1"}, {"Kolja", "Deutschland", "Rammstein", "2"}, {"Kolja", "Supermodel", "Eisbrecher", "3"}, {"Kolja", "Du Riechst So Gut", "Rammstein", "4"}, {"Kolja", "Hells Bells", "AC/DC", "5"}, {"Lars", "Seek & Destroy", "Metallica", "1"}, {"Lars", "Deutschland", "Rammstein", "2"}, {"Lars", "Seek & Destroy", "Metallica", "3"}, {"Lars", "Back in Black", "AC/DC", "4"}, {"Lars", "Seek & Destroy", "Metallica", "5"}, {"Lea", "Chronic Slaughter", "Slaughter to Prevail", "1"}, {"Lea", "Chronic Slaughter", "Slaughter to Prevail", "2"}, {"Lea", "Chronic Slaughter", "Slaughter to Prevail", "3"}, {"Lea", "Chronic Slaughter", "Slaughter to Prevail", "4"}, {"Lea", "Chronic Slaughter", "Slaughter to Prevail", "5"}, {"Luc", "Enter Sandman", "Metallica", "1"}, {"Luc", "Enter Sandman", "Metallica", "2"}, {"Luc", "Feuer Frei", "Rammstein", "3"}, {"Luc", "Feuer Frei", "Rammstein", "4"}, {"Luc", "Deutschland", "Rammstein", "5"}, {"Luca", "Back in Black", "AC/DC", "1"}, {"Luca", "Die, Die My Darling", "Metallica", "2"}, {"Luca", "Sonne", "Rammstein", "3"}, {"Luca", "Feuer Frei", "Rammstein", "4"}, {"Luca", "Feuer Frei", "Rammstein", "5"}, {"Lukas", "Chronic Slaughter", "Slaughter to Prevail", "1"}, {"Lukas", "Rammlied", "Rammstein", "2"}, {"Lukas", "Rammlied", "Rammstein", "3"}, {"Lukas", "Sabbra Cadabra", "Metallica", "4"}, {"Lukas", "Decadence", "Disturbed", "5"}, {"Maximilian P.", "Back in Black", "AC/DC", "1"}, {"Maximilian P.", "Back in Black", "AC/DC", "2"}, {"Maximilian P.", "Back in Black", "AC/DC", "3"}, {"Maximilian P.", "Highway To Hell", "AC/DC", "4"}, {"Maximilian P.", "Highway To Hell", "AC/DC", "5"}, {"Maximilian S.", "Deutschland", "Rammstein", "1"}, {"Maximilian S.", "Hells Bells", "AC/DC", "2"}, {"Maximilian S.", "Dreamer", "Ozzy Osbourne", "3"}, {"Maximilian S.", "Fire in the Sky", "Ozzy Osbourne", "4"}, {"Maximilian S.", "Fire in the Sky", "Ozzy Osbourne", "5"}, {"Nikolas", "Du Riechst So Gut", "Rammstein", "1"}, {"Nikolas", "Sonne", "Rammstein", "2"}, {"Nikolas", "Mann Gegen Mann", "Rammstein", "3"}, {"Nikolas", "Zwitter", "Rammstein", "4"}, {"Nikolas", "Zwitter", "Rammstein", "5"}, {"Pascal", "Down With the Sickness", "Disturbed", "1"}, {"Pascal", "Decadence", "Disturbed", "2"}, {"Pascal", "Master of Puppets", "Metallica", "3"}, {"Pascal", "Chronic Slaughter", "Slaughter to Prevail", "4"}, {"Pascal", "Chronic Slaughter", "Slaughter to Prevail", "5"}, {"Pavel", "Die Engel", "Eisbrecher", "1"}, {"Pavel", "Supermodel", "Eisbrecher", "2"}, {"Pavel", "Back in Black", "AC/DC", "3"}, {"Pavel", "Sonne", "Rammstein", "4"}, {"Pavel", "You Shook Me All Night Long", "AC/DC", "5"}, {"Philipp", "Crazy Train", "Ozzy Osbourne", "1"}, {"Philipp", "Whiskey in the Jar", "Metallica", "2"}, {"Philipp", "Whiskey in the Jar", "Metallica", "3"}, {"Philipp", "Deutschland", "Rammstein", "4"}, {"Philipp", "Deutschland", "Rammstein", "5"}, {"Saarujan", "Crazy Train", "Ozzy Osbourne", "1"}, {"Saarujan", "Crazy Train", "Ozzy Osbourne", "2"}, {"Saarujan", "Crazy Train", "Ozzy Osbourne", "3"}, {"Saarujan", "Fire in the Sky", "Ozzy Osbourne", "4"}, {"Saarujan", "Fire in the Sky", "Ozzy Osbourne", "5"}, {"Temuujin", "Back in Black", "AC/DC", "1"}, {"Temuujin", "Back in Black", "AC/DC", "2"}, {"Temuujin", "Back in Black", "AC/DC", "3"}, {"Temuujin", "Back in Black", "AC/DC", "4"}, {"Temuujin", "Back in Black", "AC/DC", "5"}, {"Tim W.", "Deutschland", "Rammstein", "1"}, {"Tim W.", "Deutschland", "Rammstein", "2"}, {"Tim W.", "Eiszeit", "Eisbrecher", "3"}, {"Tim W.", "Eiszeit", "Eisbrecher", "4"}, {"Tim W.", "Waidmanns Heil", "Rammstein", "5"}, {"Tom", "Army of the Night", "Powerwolf", "1"}, {"Tom", "Army of the Night", "Powerwolf", "2"}, {"Tom", "Sanctified with Dynamite", "Powerwolf", "3"}, {"Tom", "Deutschland", "Rammstein", "4"}, {"Tom", "Sonne", "Rammstein", "5"}};
	
	// Array mit Testdaten für die Gastgeber
	private static String[]		gastGeber		= {"Lars", "Gastgeber"};
	
	// Methode, die die Testdaten in der Datenbank speichert
	public static void beispielDatenLaden() {
		// Speichern der Gäste
		try {
			// JDBC-Treiber laden
			Class.forName(BeispielDaten.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(BeispielDaten.url, BeispielDaten.user, BeispielDaten.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT IGNORE INTO `T_Gast`(`name`) VALUES ";
			for (int i = 0; i < BeispielDaten.gaeste.length; i++) {
				sqlBefehl += "('" + BeispielDaten.gaeste[i] + "')";
				if (i < BeispielDaten.gaeste.length - 1)
					sqlBefehl += ", ";
			}
			sqlBefehl += ";";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
			// Fehlermeldung ausgeben
			ex.printStackTrace();
		}
		// Speichern der Gastgeber
		try {
			// JDBC-Treiber laden
			Class.forName(BeispielDaten.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(BeispielDaten.url, BeispielDaten.user, BeispielDaten.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT IGNORE INTO `T_Gastgeber`(`name`) VALUES ";
			for (int i = 0; i < BeispielDaten.gastGeber.length; i++) {
				sqlBefehl += "('" + BeispielDaten.gastGeber[i] + "')";
				if (i < BeispielDaten.gastGeber.length - 1)
					sqlBefehl += ", ";
			}
			sqlBefehl += ";";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
			// Fehlermeldung ausgeben
			ex.printStackTrace();
		}
		// Speichern der Musikwünsche
		try {
			// JDBC-Treiber laden
			Class.forName(BeispielDaten.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(BeispielDaten.url, BeispielDaten.user, BeispielDaten.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT IGNORE INTO `T_Musikwunsch`(`titelName`, `bandName`, `genre`) VALUES ";
			for (int i = 0; i < BeispielDaten.musikwunsch.length; i++) {
				sqlBefehl += "(";
				for (int j = 0; j < BeispielDaten.musikwunsch[i].length; j++) {
					sqlBefehl += "'" + BeispielDaten.musikwunsch[i][j] + "'";
					if (j < BeispielDaten.musikwunsch[i].length - 1)
						sqlBefehl += ", ";
				}
				sqlBefehl += ")";
				if (i < BeispielDaten.musikwunsch.length - 1)
					sqlBefehl += ", ";
			}
			sqlBefehl += ";";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			// Fehlermeldung ausgeben
		}
		// Speichern der Votes
		try {
			// JDBC-Treiber laden
			Class.forName(BeispielDaten.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(BeispielDaten.url, BeispielDaten.user, BeispielDaten.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT IGNORE INTO `T_Gast_Musikwunsch`(`name`,`titelName`, `bandName`, `voteNr`) VALUES ";
			for (int i = 0; i < BeispielDaten.gastMusikwunsch.length; i++) {
				sqlBefehl += "(";
				for (int j = 0; j < BeispielDaten.gastMusikwunsch[i].length; j++) {
					sqlBefehl += "'" + BeispielDaten.gastMusikwunsch[i][j] + "'";
					if (j < BeispielDaten.gastMusikwunsch[i].length - 1)
						sqlBefehl += ", ";
				}
				sqlBefehl += ")";
				if (i < BeispielDaten.gastMusikwunsch.length - 1)
					sqlBefehl += ", ";
			}
			sqlBefehl += ";";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
			// Fehlermeldung ausgeben
			ex.printStackTrace();
		}
	}
	
	
}
