package data;

// Klasse für Musikwünsche
public class Musikwunsch implements Comparable<Object> {
	
	// Attribute eines Musikwunsches
	private String			titelName;
	private String			bandName;
	private String			genre;
	private MySQLDatabase	database;
	
	// Konstruktor
	public Musikwunsch(String titelName, String bandName, String genre, MySQLDatabase database) {
		super();
		this.setTitelName(titelName);
		this.setBandName(bandName);
		this.setGenre(genre);
		this.setDatabase(database);
	}
	
	@Override // Zum sortieren nach Anzahl von Votes
	public int compareTo(Object obj) {
		Musikwunsch other = (Musikwunsch) obj;
		return other.getVotes() - this.getVotes();
	}
	
	@Override // Methode zum Feststellen ob zwei Musikwünsche identisch sind
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Musikwunsch other = (Musikwunsch) obj;
		if (this.getBandName() == null) {
			if (other.getBandName() != null)
				return false;
		} else if (!this.getBandName().equals(other.getBandName()))
			return false;
		if (this.getGenre() == null) {
			if (other.getGenre() != null)
				return false;
		} else if (!this.getGenre().equals(other.getGenre()))
			return false;
		if (this.getTitelName() == null) {
			if (other.getTitelName() != null)
				return false;
		} else if (!this.getTitelName().equals(other.getTitelName()))
			return false;
		return true;
	}
	
	@Override // Methode, die die Klasse als String ausgibt
	public String toString() {
		return "\tMusikwunsch [titelName=" + this.getTitelName() + ", bandName=" + this.getBandName() + ", genre=" + this.getGenre() + ", votes= " + this.getVotes() + "]\n";
	}
	
	// Getter und Setter
	
	// Getter für den Namen der Band
	public String getBandName() {
		return this.bandName;
	}
	
	// Getter für die Datenbank-Klasse
	public MySQLDatabase getDatabase() {
		return this.database;
	}
	
	// Getter für das Genre
	public String getGenre() {
		return this.genre;
	}
	
	// Getter für den Titel
	public String getTitelName() {
		return this.titelName;
	}
	
	// Getter für die Votes
	public int getVotes() {
		return this.database.calculateVotes(this);
	}
	
	// Setter für den Namen der Band
	public void setBandName(String bandName) {
		this.bandName = bandName;
	}
	
	// Setter für die Datenbank
	public void setDatabase(MySQLDatabase database) {
		this.database = database;
	}
	
	// Setter für das Genre
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	// Setter für den Titel
	public void setTitelName(String titelName) {
		this.titelName = titelName;
	}
}
