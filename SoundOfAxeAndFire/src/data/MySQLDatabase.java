package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

// Klasse, die die Verbindung zur Datenbank darstellt
public class MySQLDatabase {
	
	// Verbindungsinformationen
	private String	driver		= "com.mysql.jdbc.Driver";
	private String	url			= "jdbc:mysql://localhost/SoundOfAxeAndFire?";
	private String	user		= "root";
	private String	password	= "";
	
	// Konstruktor
	public MySQLDatabase() {
		super();
		this.setup();
	}
	
	// Methode zum Hinzufügen eines Gastes
	public boolean addGast(String name) {
		Boolean done = false;
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT INTO `T_Gast`(`name`) VALUES (?);";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// Daten eintragen
			stmt.setString(1, name);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
			done = true;
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return done;
	}
	
	// Methode zum Hinzufügen eines Musikwunsches
	public boolean addMusikwunsch(Musikwunsch musikwunsch) {
		Boolean done = false;
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT INTO `T_Musikwunsch`(`titelName`, `bandName`, `genre`) VALUES (?, ?, ?);";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// Daten eintragen
			stmt.setString(1, musikwunsch.getTitelName());
			stmt.setString(2, musikwunsch.getBandName());
			stmt.setString(3, musikwunsch.getGenre());
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
			done = true;
		} catch (Exception ex) { // Fehler abfangen
			//ex.printStackTrace();// Fehlermeldung ausgeben
			System.err.println("Eintrag bereits vorhanden!");
		}
		return done;
	}
	
	// Methode zum Hinzufügen eines Votes
	public boolean addVote(String name, Musikwunsch musikwunsch) {
		Boolean done = false;
		// Löschen der Votes, die nicht zu den letzten 4 gehören
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "DELETE FROM `T_Gast_Musikwunsch` WHERE name = ? AND voteNr <= ?;";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// Daten eintragen
			stmt.setString(1, name);
			stmt.setInt(2, this.getLastVoteNr(name) - 4);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		// Hinzufügen des neuesten Votes
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "INSERT INTO `T_Gast_Musikwunsch`(`name`, `titelName`, `bandName`, `voteNr`) VALUES (?, ?, ?, ?);";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// Daten eintragen
			stmt.setString(1, name);
			stmt.setString(2, musikwunsch.getTitelName());
			stmt.setString(3, musikwunsch.getBandName());
			stmt.setInt(4, this.getLastVoteNr(name) + 1);
			// SQL-Anweisungen ausführen
			stmt.executeUpdate();
			// Verbindung schließen
			con.close();
			done = true;
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return done;
	}
	
	// Methode zum Berechnen der Votes
	public int calculateVotes(Musikwunsch musikwunsch) {
		int votes = 0;
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "SELECT COUNT(titelName) AS Votes FROM t_gast_musikwunsch WHERE voteNr > 0 AND titelName = ? AND bandName = ? GROUP BY titelName LIMIT 1";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// Daten eintragen
			stmt.setString(1, musikwunsch.getTitelName());
			stmt.setString(2, musikwunsch.getBandName());
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery();
			// Ergebnis verarbeiten
			if (rs.next())
				votes = rs.getInt("Votes");
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return votes;
	}
	
	// Methode zur Ausgabe des Gastgebers
	public List<String> getGastgeberNamen() {
		List<String> namen = new ArrayList<>();
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "SELECT * FROM T_Gastgeber;";
			// SQL-Anweisungen erstellen
			Statement stmt = con.createStatement();
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery(sqlBefehl);
			// Ergebnis verarbeiten
			while (rs.next()) {
				String name = rs.getString("name");
				namen.add(name);
			}
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return namen;
	}
	
	// Methode zum Herausfinden der letzten VoteNr eines Gastes
	public int getLastVoteNr(String name) {
		int lastVoteNr = 0;
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "SELECT MAX(voteNr) AS `maxVoteNr`, name FROM T_Gast_Musikwunsch WHERE name = ?;";
			// SQL-Anweisungen erstellen
			PreparedStatement stmt = con.prepareStatement(sqlBefehl);
			// Daten eintragen
			stmt.setString(1, name);
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery();
			// Verbindung schließen
			if (rs.next())
				lastVoteNr = rs.getInt("maxVoteNr");
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return lastVoteNr;
	}
	
	// Methode zur Ausgabe der Gästeliste
	public List<String> getNamen() {
		List<String> namen = new ArrayList<>();
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "SELECT * FROM T_Gast;";
			// SQL-Anweisungen erstellen
			Statement stmt = con.createStatement();
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery(sqlBefehl);
			// Ergebnis verarbeiten
			while (rs.next()) {
				String name = rs.getString("name");
				namen.add(name);
			}
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return namen;
	}
	
	// Methode zur Ausgabe der Musikwunschliste (Playlist)
	public List<Musikwunsch> getPlaylist() {
		List<Musikwunsch> playlist = new ArrayList<>();
		try {
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(this.url, this.user, this.password);
			// SQL-Befehl deklarieren
			String sqlBefehl = "SELECT * FROM T_Musikwunsch;";
			// SQL-Anweisungen erstellen
			Statement stmt = con.createStatement();
			// SQL-Anweisungen ausführen
			ResultSet rs = stmt.executeQuery(sqlBefehl);
			// Ergebnis verarbeiten
			while (rs.next()) {
				Musikwunsch newMusikwunsch = new Musikwunsch(rs.getString("titelName"), rs.getString("bandName"), rs.getString("genre"), this);
				playlist.add(newMusikwunsch);
			}
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		return playlist;
	}
	
	// Methode, die Beispieldaten lädt
	public void ladeBeispielDaten() {
		BeispielDaten.beispielDatenLaden();
	}
	
	// Methode zum Zurücksetzen der Datenbank
	public void reset() {
		try {
			// DataBase erstellen
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection("jdbc:mysql://localhost/?", this.user, this.password);
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			stmt.executeUpdate("DROP DATABASE IF EXISTS `SoundOfAxeAndFire`;");
			// Verbindung schließen
			con.close();
		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
		this.setup();
	}
	
	// Erstellt ggf. eine Datenbank mit dazugehörigen Tabellen
	public void setup() {
		try {
			// DataBase erstellen
			// JDBC-Treiber laden
			Class.forName(this.driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection("jdbc:mysql://localhost/?", this.user, this.password);
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS SoundOfAxeAndFire;");
			// Verbindung schließen
			con.close();
			
			// Tabelle für Musikwünsche erstellen
			// Verbindung aufbauen
			con = DriverManager.getConnection(this.url, this.user, this.password);
			stmt = con.createStatement();
			// Kundentabelle einfügen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Musikwunsch(titelName VARCHAR(100) NOT NULL, bandName VARCHAR(100) NOT NULL, genre VARCHAR(50) NOT NULL, PRIMARY KEY (titelName, bandName));");
			// Verbindung schließen
			con.close();
			
			// Tabelle für Gäste erstellen
			// Verbindung aufbauen
			con = DriverManager.getConnection(this.url, this.user, this.password);
			stmt = con.createStatement();
			// Kundentabelle einfügen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Gast(name VARCHAR(50) NOT NULL, PRIMARY KEY (name));");
			// Verbindung schließen
			con.close();
			
			// Tabelle für Votes (Gast_Musikwunsch) erstellen
			// Verbindung aufbauen
			con = DriverManager.getConnection(this.url, this.user, this.password);
			stmt = con.createStatement();
			// Kundentabelle einfügen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS T_Gast_Musikwunsch(name VARCHAR(50) NOT NULL, titelName VARCHAR(100) NOT NULL, bandName VARCHAR(100) NOT NULL, voteNr INT NOT NULL, PRIMARY KEY (name, titelName, bandName, voteNr), FOREIGN KEY (name) REFERENCES T_Gast (name), FOREIGN KEY (titelName, bandName) REFERENCES T_Musikwunsch (titelName, bandName));");
			// Verbindung schließen
			con.close();
			
			// Tabelle für Gastgeber erstellen
			// Verbindung aufbauen
			con = DriverManager.getConnection(this.url, this.user, this.password);
			stmt = con.createStatement();
			// Kundentabelle einfügen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS `T_Gastgeber`(`name` VARCHAR(50) NOT NULL, CONSTRAINT pk_Gastgeber PRIMARY KEY (`name`), CONSTRAINT fk_Gastgeber FOREIGN KEY (`name`) REFERENCES T_Gast (`name`) ON DELETE CASCADE ON UPDATE CASCADE);");
			// Verbindung schließen
			con.close();
			
		} catch (Exception ex) { // Fehler abfangen
			//ex.printStackTrace();// Fehlermeldung ausgeben
			System.err.println("Server Verbindung nicht gefunden!!");
			System.err.println("    Bitte starten Sie XAMPP!");
			System.exit(0);
		}
	}
	
}
