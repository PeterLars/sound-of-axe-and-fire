package view;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import logic.Playlist;

// Oberfläche zur Ausgabe der nach Votes sortierten Playlist
public class PlaylistPanel extends JPanel {
	
	// Attribute
	private static final long	serialVersionUID	= 1L;
	private KopfzeilenPanel		pnl_kopfzeile;
	private JTable				table;
	private Hauptfenster		hauptfenster;
	private JButton				btn_Zurueck;
	private JScrollPane			scrollPane;
	private JButton				btn_PlaylistLoeschen;
	
	// Konstruktor
	public PlaylistPanel(Hauptfenster hauptfenster) {
		this.setBackground(Color.BLACK);
		this.hauptfenster = hauptfenster;
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0};
		this.setLayout(gridBagLayout);
		// Knopf, der einem zur Vote Oberfläche zurück führt
		this.btn_Zurueck = new JButton("Zurück");
		this.btn_Zurueck.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.btn_Zurueck.setBackground(Color.WHITE);
		this.btn_Zurueck.addActionListener(e -> hauptfenster.goTo("pnl_Vote"));
		// Fügt die Kopfzeile ein
		this.pnl_kopfzeile = new KopfzeilenPanel(hauptfenster);
		GridBagConstraints gbc_pnl_kopfzeile = new GridBagConstraints();
		gbc_pnl_kopfzeile.insets = new Insets(0, 0, 10, 0);
		gbc_pnl_kopfzeile.anchor = GridBagConstraints.NORTH;
		gbc_pnl_kopfzeile.fill = GridBagConstraints.HORIZONTAL;
		gbc_pnl_kopfzeile.gridx = 0;
		gbc_pnl_kopfzeile.gridy = 0;
		this.add(this.pnl_kopfzeile, gbc_pnl_kopfzeile);
		// Neues ScrollPane für die JTable
		this.scrollPane = new JScrollPane();
		this.scrollPane.setBackground(Color.BLACK);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 10, 0);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		this.add(this.scrollPane, gbc_scrollPane);
		// Erzeugen der JTable
		this.table = new JTable();
		this.table.setSelectionBackground(Color.BLACK);
		this.table.setSelectionForeground(Color.WHITE);
		this.table.setName("");
		this.table.setGridColor(Color.WHITE);
		this.table.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		this.table.setAutoCreateRowSorter(true);
		this.table.setRowSelectionAllowed(false);
		this.table.setFillsViewportHeight(true);
		this.table.setShowVerticalLines(false);
		this.table.setEnabled(false);
		this.table.setFocusable(false);
		this.table.setFocusTraversalKeysEnabled(false);
		this.table.setRowHeight(30);
		this.table.setForeground(Color.WHITE);
		this.table.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.table.setBackground(Color.BLACK);
		this.scrollPane.setViewportView(this.table);
		// Knopf, der dazu dient: Die Playlist zu löschen
		this.btn_PlaylistLoeschen = new JButton("Playlist Löschen");
		this.btn_PlaylistLoeschen.setForeground(Color.WHITE);
		this.btn_PlaylistLoeschen.addActionListener(arg0 -> {
			// Fordert den Gastgeber dazu auf die Löschung zu bestätigen
			if (PlaylistPanel.this.btn_PlaylistLoeschen.getText().equals("Bestätigen")) {
				PlaylistPanel.this.btn_PlaylistLoeschen.setText("Playlist Löschen");
				this.btn_PlaylistLoeschen.setBackground(new Color(128, 0, 0));
				hauptfenster.reset();
			} else {
				PlaylistPanel.this.btn_PlaylistLoeschen.setBackground(Color.RED);
				PlaylistPanel.this.btn_PlaylistLoeschen.setText("Bestätigen");
			}
		});
		this.btn_PlaylistLoeschen.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.btn_PlaylistLoeschen.setBackground(new Color(128, 0, 0));
		GridBagConstraints gbc_btn_PlaylistLoeschen = new GridBagConstraints();
		gbc_btn_PlaylistLoeschen.fill = GridBagConstraints.BOTH;
		gbc_btn_PlaylistLoeschen.insets = new Insets(0, 0, 5, 0);
		gbc_btn_PlaylistLoeschen.gridx = 0;
		gbc_btn_PlaylistLoeschen.gridy = 2;
		this.add(this.btn_PlaylistLoeschen, gbc_btn_PlaylistLoeschen);
		GridBagConstraints gbc_btn_Zurueck = new GridBagConstraints();
		gbc_btn_Zurueck.anchor = GridBagConstraints.NORTH;
		gbc_btn_Zurueck.fill = GridBagConstraints.HORIZONTAL;
		gbc_btn_Zurueck.gridx = 0;
		gbc_btn_Zurueck.gridy = 3;
		this.add(this.btn_Zurueck, gbc_btn_Zurueck);
	}
	
	// Methode zum Aktualisieren
	public void update() {
		this.pnl_kopfzeile.update();
		this.table.setModel(new DefaultTableModel(this.hauptfenster.getPlaylist().getVotedJTable(), Playlist.SPALTENNAMEN));
		this.scrollPane.setViewportView(this.table);
	}
	
}
