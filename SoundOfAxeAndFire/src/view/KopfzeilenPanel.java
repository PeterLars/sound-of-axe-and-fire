package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

// Oberfläche für die Kopfzeile
public class KopfzeilenPanel extends JPanel {
	
	// Attribute
	private static final long	serialVersionUID	= 1L;
	private JTextField			txf_Name;
	private Hauptfenster		hauptfenster;
	private JButton				btn_Playlist;
	private JButton				btn_Logout;
	
	// Konstruktor
	public KopfzeilenPanel(Hauptfenster hauptfenster) {
		this.hauptfenster = hauptfenster;
		this.setBackground(Color.BLACK);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0};
		gridBagLayout.columnWeights = new double[]{2.0, 3.0, 1.0};
		gridBagLayout.rowWeights = new double[]{0.0};
		this.setLayout(gridBagLayout);
		// Textfeld mit dem Namen des Gastes
		this.txf_Name = new JTextField();
		this.txf_Name.setForeground(Color.BLACK);
		this.txf_Name.setEditable(false);
		this.txf_Name.setToolTipText("Dein Name");
		this.txf_Name.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.txf_Name.setBackground(Color.WHITE);
		this.txf_Name.setText("        ");
		GridBagConstraints gbc_txf_Name = new GridBagConstraints();
		gbc_txf_Name.fill = GridBagConstraints.BOTH;
		gbc_txf_Name.insets = new Insets(0, 0, 0, 10);
		gbc_txf_Name.gridx = 0;
		gbc_txf_Name.gridy = 0;
		this.add(this.txf_Name, gbc_txf_Name);
		this.txf_Name.setColumns(1);
		// Button der einem zur Playlist Ansicht führt  
		this.btn_Playlist = new JButton("Playlist ausgeben");
		this.btn_Playlist.setForeground(Color.BLACK);
		this.btn_Playlist.setToolTipText("führt dich zur Playlist");
		this.btn_Playlist.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.btn_Playlist.setEnabled(false);
		this.btn_Playlist.addActionListener(arg0 -> hauptfenster.goTo("pnl_Playlist"));
		this.btn_Playlist.setBackground(Color.WHITE);
		GridBagConstraints gbc_btn_Playlist = new GridBagConstraints();
		gbc_btn_Playlist.fill = GridBagConstraints.BOTH;
		gbc_btn_Playlist.insets = new Insets(0, 0, 0, 10);
		gbc_btn_Playlist.gridx = 1;
		gbc_btn_Playlist.gridy = 0;
		this.add(this.btn_Playlist, gbc_btn_Playlist);
		// Button zum Ausloggen
		this.btn_Logout = new JButton("Logout");
		this.btn_Logout.setForeground(Color.BLACK);
		this.btn_Logout.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.btn_Logout.setToolTipText("Logout");
		this.btn_Logout.addActionListener(arg0 -> hauptfenster.goTo("pnl_Login"));
		this.btn_Logout.setBackground(Color.WHITE);
		GridBagConstraints gbc_btn_Logout = new GridBagConstraints();
		gbc_btn_Logout.fill = GridBagConstraints.BOTH;
		gbc_btn_Logout.gridx = 2;
		gbc_btn_Logout.gridy = 0;
		this.add(this.btn_Logout, gbc_btn_Logout);
	}
	
	// Methode zum Aktualisieren
	public void update() {
		this.txf_Name.setText(this.hauptfenster.getGastName());
		this.btn_Playlist.setEnabled(this.hauptfenster.getIsGastgeber());
	}
	
}
