package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import data.Musikwunsch;

// Oberfläche zum Hinzufügen neuer Musikwünsche
public class MusikwunschPanel extends JPanel {
	
	// Attribute
	private static final long	serialVersionUID	= 1L;
	private JTextField			txf_TietelName;
	private JTextField			txf_BandName;
	private JTextField			txf_Genre;
	private KopfzeilenPanel		kopfzeile;
	
	// Konstruktor
	public MusikwunschPanel(Hauptfenster hauptfenster) {
		this.setBackground(Color.BLACK);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0};
		this.setLayout(gridBagLayout);
		// Kopfzeile hinzufügen
		this.kopfzeile = new KopfzeilenPanel(hauptfenster);
		GridBagConstraints gbc_kopfzeile = new GridBagConstraints();
		gbc_kopfzeile.anchor = GridBagConstraints.NORTH;
		gbc_kopfzeile.fill = GridBagConstraints.HORIZONTAL;
		gbc_kopfzeile.insets = new Insets(0, 0, 10, 0);
		gbc_kopfzeile.gridx = 0;
		gbc_kopfzeile.gridy = 0;
		this.add(this.kopfzeile, gbc_kopfzeile);
		// Mittleres Panel
		JPanel pnl_Mitte = new JPanel();
		pnl_Mitte.setBackground(Color.BLACK);
		GridBagConstraints gbc_pnl_Mitte = new GridBagConstraints();
		gbc_pnl_Mitte.fill = GridBagConstraints.BOTH;
		gbc_pnl_Mitte.insets = new Insets(0, 0, 10, 0);
		gbc_pnl_Mitte.gridx = 0;
		gbc_pnl_Mitte.gridy = 1;
		this.add(pnl_Mitte, gbc_pnl_Mitte);
		GridBagLayout gbl_pnl_Mitte = new GridBagLayout();
		gbl_pnl_Mitte.columnWidths = new int[]{0, 300};
		gbl_pnl_Mitte.rowHeights = new int[]{60, 60, 60};
		gbl_pnl_Mitte.columnWeights = new double[]{0.0, 1.0};
		gbl_pnl_Mitte.rowWeights = new double[]{0.0, 0.0, 0.0};
		pnl_Mitte.setLayout(gbl_pnl_Mitte);
		// Label für den Titelnamen:
		JLabel lbl_TitelName = new JLabel("Titelname :");
		lbl_TitelName.setForeground(Color.WHITE);
		lbl_TitelName.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lbl_TitelName = new GridBagConstraints();
		gbc_lbl_TitelName.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_TitelName.insets = new Insets(0, 0, 10, 10);
		gbc_lbl_TitelName.gridx = 0;
		gbc_lbl_TitelName.gridy = 0;
		pnl_Mitte.add(lbl_TitelName, gbc_lbl_TitelName);
		// Textfeld für den Titelname
		this.txf_TietelName = new JTextField();
		this.txf_TietelName.setForeground(Color.BLACK);
		this.txf_TietelName.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_txf_TietelName = new GridBagConstraints();
		gbc_txf_TietelName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_TietelName.insets = new Insets(2, 0, 4, 0);
		gbc_txf_TietelName.gridx = 1;
		gbc_txf_TietelName.gridy = 0;
		pnl_Mitte.add(this.txf_TietelName, gbc_txf_TietelName);
		this.txf_TietelName.setColumns(10);
		// Label für den Bandnamen
		JLabel lbl_BandName = new JLabel("Bandname:");
		lbl_BandName.setForeground(Color.WHITE);
		lbl_BandName.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lbl_BandName = new GridBagConstraints();
		gbc_lbl_BandName.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_BandName.insets = new Insets(0, 0, 10, 10);
		gbc_lbl_BandName.gridx = 0;
		gbc_lbl_BandName.gridy = 1;
		pnl_Mitte.add(lbl_BandName, gbc_lbl_BandName);
		// Textfeld für den Bandnamen
		this.txf_BandName = new JTextField();
		this.txf_BandName.setForeground(Color.BLACK);
		this.txf_BandName.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_txf_BandName = new GridBagConstraints();
		gbc_txf_BandName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_BandName.insets = new Insets(2, 0, 2, 0);
		gbc_txf_BandName.gridx = 1;
		gbc_txf_BandName.gridy = 1;
		pnl_Mitte.add(this.txf_BandName, gbc_txf_BandName);
		this.txf_BandName.setColumns(10);
		// Label für das Genre
		JLabel lbl_Genre = new JLabel("Genre       :");
		lbl_Genre.setForeground(Color.WHITE);
		lbl_Genre.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_lbl_Genre = new GridBagConstraints();
		gbc_lbl_Genre.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_Genre.insets = new Insets(0, 0, 0, 10);
		gbc_lbl_Genre.gridx = 0;
		gbc_lbl_Genre.gridy = 2;
		pnl_Mitte.add(lbl_Genre, gbc_lbl_Genre);
		// Textfeld für das Genre
		this.txf_Genre = new JTextField();
		this.txf_Genre.setForeground(Color.BLACK);
		this.txf_Genre.setFont(new Font("Tahoma", Font.PLAIN, 25));
		GridBagConstraints gbc_txf_Genre = new GridBagConstraints();
		gbc_txf_Genre.insets = new Insets(4, 0, 2, 0);
		gbc_txf_Genre.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_Genre.gridx = 1;
		gbc_txf_Genre.gridy = 2;
		pnl_Mitte.add(this.txf_Genre, gbc_txf_Genre);
		this.txf_Genre.setColumns(10);
		// Unteres Panel
		JPanel pnl_Unten = new JPanel();
		pnl_Unten.setBackground(Color.BLACK);
		GridBagConstraints gbc_pnl_Unten = new GridBagConstraints();
		gbc_pnl_Unten.anchor = GridBagConstraints.NORTH;
		gbc_pnl_Unten.fill = GridBagConstraints.HORIZONTAL;
		gbc_pnl_Unten.gridx = 0;
		gbc_pnl_Unten.gridy = 2;
		this.add(pnl_Unten, gbc_pnl_Unten);
		GridBagLayout gbl_pnl_Unten = new GridBagLayout();
		gbl_pnl_Unten.columnWidths = new int[]{0, 0, 0};
		gbl_pnl_Unten.rowHeights = new int[]{0};
		gbl_pnl_Unten.columnWeights = new double[]{1.0, 3.0, Double.MIN_VALUE};
		gbl_pnl_Unten.rowWeights = new double[]{1.0};
		pnl_Unten.setLayout(gbl_pnl_Unten);
		// Knopf zum Hinzufügen eines Musikwunsches
		JButton btn_Hinzufuegen = new JButton("Hinzufügen");
		btn_Hinzufuegen.setForeground(Color.BLACK);
		btn_Hinzufuegen.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_Hinzufuegen.setBackground(Color.WHITE);
		btn_Hinzufuegen.addActionListener(e -> {
			Musikwunsch newMusikwunsch = new Musikwunsch(MusikwunschPanel.this.txf_TietelName.getText(), MusikwunschPanel.this.txf_BandName.getText(), MusikwunschPanel.this.txf_Genre.getText(), hauptfenster.getPlaylist().getMySQLDatabase());
			hauptfenster.getPlaylist().addMusikwunsch(newMusikwunsch);
			MusikwunschPanel.this.txf_BandName.setText("");
			MusikwunschPanel.this.txf_Genre.setText("");
			MusikwunschPanel.this.txf_TietelName.setText("");
		});
		// Knopf, der einem zur Vote Oberfläche zurück führt
		JButton btn_Zurueck = new JButton("Zurück");
		btn_Zurueck.setForeground(Color.BLACK);
		btn_Zurueck.setFont(new Font("Tahoma", Font.PLAIN, 25));
		btn_Zurueck.setBackground(Color.WHITE);
		btn_Zurueck.addActionListener(arg0 -> hauptfenster.goTo("pnl_Vote"));
		GridBagConstraints gbc_btn_Zurueck = new GridBagConstraints();
		gbc_btn_Zurueck.fill = GridBagConstraints.BOTH;
		gbc_btn_Zurueck.insets = new Insets(0, 0, 0, 10);
		gbc_btn_Zurueck.gridx = 0;
		gbc_btn_Zurueck.gridy = 0;
		pnl_Unten.add(btn_Zurueck, gbc_btn_Zurueck);
		GridBagConstraints gbc_btn_Hinzufuegen = new GridBagConstraints();
		gbc_btn_Hinzufuegen.fill = GridBagConstraints.BOTH;
		gbc_btn_Hinzufuegen.gridx = 1;
		gbc_btn_Hinzufuegen.gridy = 0;
		pnl_Unten.add(btn_Hinzufuegen, gbc_btn_Hinzufuegen);
		
	}
	
	// Methode zum Aktualisieren
	public void update() {
		this.kopfzeile.update();
	}
	
}
