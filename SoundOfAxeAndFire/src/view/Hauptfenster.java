package view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import logic.Playlist;

// Eine Klasse, die alle grafischen Elemente miteinander verbindet und Logicteil der GUI übernimmt
public class Hauptfenster extends JFrame {
	
	// Attribute
	private static final long	serialVersionUID	= 1L;
	private Playlist			playlist;
	private JPanel				pnl_Hintergrund;
	private CardLayout			cardLayout;
	private LoginPanel			pnl_Login;
	private VoteAnsichtsPanel	pnl_Vote;
	private PlaylistPanel		pnl_Playlist;
	private MusikwunschPanel	pnl_Musikwunsch;
	private String				gastName;
	private boolean				isGastgeber			= false;
	
	
	// Startet die Anwendung
	public static void main(String[] args) {
		EventQueue.invokeLater(() -> {
			try {
				Playlist playlist = new Playlist();
				Hauptfenster frame = new Hauptfenster(playlist);
				frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	// Erstellt das Fenster
	public Hauptfenster(Playlist playlist) {
		this.setTitle("Sound of Axe and Fire");
		this.playlist = playlist;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setBounds(0, 0, 800, 500);
		this.setLocationRelativeTo(null);
		this.pnl_Hintergrund = new JPanel();
		this.pnl_Hintergrund.setBackground(Color.BLACK);
		this.pnl_Hintergrund.setBorder(new EmptyBorder(10, 10, 10, 10));
		this.setContentPane(this.pnl_Hintergrund);
		this.pnl_Hintergrund.setLayout(this.cardLayout = new CardLayout(0, 0));
		
		// Legt die Oberfläche zum Einloggen an
		this.pnl_Login = new LoginPanel(this);
		this.getContentPane().add(this.pnl_Login, "pnl_Login");
		
		// Legt die Oberfläche zum Voten an
		this.pnl_Vote = new VoteAnsichtsPanel(this);
		this.getContentPane().add(this.pnl_Vote, "pnl_Vote");
		
		// Legt die Oberfläche zum Anschauen der nach Votes sortierten Playlist an
		this.pnl_Playlist = new PlaylistPanel(this);
		this.getContentPane().add(this.pnl_Playlist, "pnl_Playlist");
		
		// Legt die Oberfläche zum Hinzufügen neuer Musikwünsche an
		this.pnl_Musikwunsch = new MusikwunschPanel(this);
		this.getContentPane().add(this.pnl_Musikwunsch, "pnl_Musikwunsch");
	}
	
	// Methode zum Voten
	public void vote(int id) {
		this.playlist.addVote(this.gastName, this.playlist.get(id));
		this.update();
	}
	
	// Methode zum Einloggen
	public void login(String name) {
		// Falls Beispiel eingegeben wurde werden die Beispieldaten geladen
		if (name.equals("Beispiel"))
			this.playlist.getMySQLDatabase().ladeBeispielDaten();
		// Falls der Gast existiert wird er weitergeleitet
		else if (this.playlist.getMySQLDatabase().getNamen().contains(name)) {
			this.isGastgeber = this.playlist.getMySQLDatabase().getGastgeberNamen().contains(name);
			this.setGastName(name);
			this.goTo("pnl_Vote");
		// Falls der Gast nicht existiert wird er registriert
		} else {
			this.playlist.addGast(name);
			this.pnl_Login.loginFailed();
		}
	}
	
	// Methode zum Navigieren zwischen den verschiedenen Oberflächen
	public void goTo(String location) {
		if (location.equals("pnl_Playlist"))
			this.pnl_Playlist.update();
		this.cardLayout.show(this.pnl_Hintergrund, location);
		this.update();
	}
	
	// Methode, die die Datenbank zurücksetzt
	public void reset() {
		this.playlist.getMySQLDatabase().reset();
		this.goTo("pnl_Login");
	}
	
	// Methode, die die Oberflächen aktualisiert
	public void update() {
		this.pnl_Musikwunsch.update();
		this.pnl_Vote.update();
	}
	
	// Getter der Playlist
	public Playlist getPlaylist() {
		return this.playlist;
	}
	
	// Überprüft ob der Gastgeber eingeloggt ist
	public boolean getIsGastgeber() {
		return this.isGastgeber;
	}
	
	// Setter des Gastnamens
	public void setGastName(String gastName) {
		this.gastName = gastName;
	}
	
	// Getter des Gastnamens
	public String getGastName() {
		return this.gastName;
	}
}
