package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

// Oberfläche zum Einloggen / Registrieren
public class LoginPanel extends JPanel {

	// Attribute
	private static final long	serialVersionUID	= 1L;
	private JTextField			txf_Name;
	private JButton				btn_Login;
	private JLabel				lbl_Name;
	private Hauptfenster		hauptfenster;
	
	// Konstruktor
	public LoginPanel(Hauptfenster hauptfenster) {
		this.hauptfenster = hauptfenster;
		this.setBackground(Color.BLACK);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{60, 150, 90, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 3.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		this.setLayout(gridBagLayout);
		// Label zum Verweis auf das Namensfeld
		this.lbl_Name = new JLabel("Name:");
		this.lbl_Name.setFont(new Font("Tahoma", Font.PLAIN, 31));
		this.lbl_Name.setForeground(Color.WHITE);
		this.lbl_Name.setBackground(Color.WHITE);
		GridBagConstraints gbc_lbl_Name = new GridBagConstraints();
		gbc_lbl_Name.fill = GridBagConstraints.HORIZONTAL;
		gbc_lbl_Name.gridwidth = 3;
		gbc_lbl_Name.insets = new Insets(0, 0, 5, 0);
		gbc_lbl_Name.gridx = 0;
		gbc_lbl_Name.gridy = 1;
		this.add(this.lbl_Name, gbc_lbl_Name);
		// Textfeld für den Namen
		this.txf_Name = new JTextField();
		this.txf_Name.setForeground(Color.BLACK);
		this.txf_Name.setBackground(Color.WHITE);
		this.txf_Name.setSelectedTextColor(Color.WHITE);
		this.txf_Name.setSelectionColor(Color.GRAY);
		this.txf_Name.setFocusCycleRoot(true);
		this.txf_Name.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == 10)
					LoginPanel.this.login();
			}
		});
		this.txf_Name.setToolTipText("gib deinen Namen ein");
		this.txf_Name.setFont(new Font("Tahoma", Font.PLAIN, 31));
		GridBagConstraints gbc_txf_Name = new GridBagConstraints();
		gbc_txf_Name.fill = GridBagConstraints.HORIZONTAL;
		gbc_txf_Name.gridwidth = 3;
		gbc_txf_Name.insets = new Insets(0, 0, 5, 0);
		gbc_txf_Name.gridx = 0;
		gbc_txf_Name.gridy = 2;
		this.add(this.txf_Name, gbc_txf_Name);
		this.txf_Name.setColumns(1);
		this.lbl_Name.setLabelFor(this.txf_Name);
		// Knopf zum einloggen / Registern
		this.btn_Login = new JButton("Login");
		this.btn_Login.setForeground(Color.BLACK);
		this.btn_Login.addActionListener(arg0 -> LoginPanel.this.login());
		this.btn_Login.setToolTipText("Login");
		this.btn_Login.setFont(new Font("Tahoma", Font.PLAIN, 31));
		this.btn_Login.setBackground(Color.WHITE);
		GridBagConstraints gbc_btn_Login = new GridBagConstraints();
		gbc_btn_Login.insets = new Insets(0, 0, 5, 0);
		gbc_btn_Login.fill = GridBagConstraints.BOTH;
		gbc_btn_Login.gridwidth = 3;
		gbc_btn_Login.gridx = 0;
		gbc_btn_Login.gridy = 3;
		this.add(this.btn_Login, gbc_btn_Login);
	}
	
	// Methode zum einloggen
	public void login() {
		this.btn_Login.setText("Login");
		this.hauptfenster.login(LoginPanel.this.txf_Name.getText());
		this.txf_Name.setText("");
	}
	
	// Methode, die Anzeigt das der Gast noch nicht registriert war
	public void loginFailed() {
		this.btn_Login.setText("Registriert");
	}
}
