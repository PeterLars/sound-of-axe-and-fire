package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import logic.JButtonColumn;
import logic.Playlist;

public class VoteAnsichtsPanel extends JPanel {
	
	// Attribute
	private static final long	serialVersionUID	= 1L;
	private JTable				table;
	private Hauptfenster		hauptfenster;
	private KopfzeilenPanel		pnl_kopfzeile;
	private JButton				btn_MusikwunschHinzufgen;
	private JScrollPane			scrollPane;
	private GridBagLayout		gridBagLayout;
	private GridBagConstraints	gbc_pnl_kopfzeile;
	private GridBagConstraints	gbc_scrollPane;
	private GridBagConstraints	gbc_btn_MusikwunschHinzufgen;
	
	// Konstruktor
	public VoteAnsichtsPanel(Hauptfenster hauptfenster) {
		this.setBackground(Color.BLACK);
		this.hauptfenster = hauptfenster;
		this.gridBagLayout = new GridBagLayout();
		this.gridBagLayout.columnWidths = new int[]{0};
		this.gridBagLayout.rowHeights = new int[]{0, 0, 0};
		this.gridBagLayout.columnWeights = new double[]{1.0};
		this.gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0};
		this.setLayout(this.gridBagLayout);
		// Einfügen der Kopfzeile
		this.pnl_kopfzeile = new KopfzeilenPanel(hauptfenster);
		this.pnl_kopfzeile.setForeground(Color.BLACK);
		this.pnl_kopfzeile.setBackground(Color.BLACK);
		this.gbc_pnl_kopfzeile = new GridBagConstraints();
		this.gbc_pnl_kopfzeile.insets = new Insets(0, 0, 10, 0);
		this.gbc_pnl_kopfzeile.anchor = GridBagConstraints.NORTH;
		this.gbc_pnl_kopfzeile.fill = GridBagConstraints.HORIZONTAL;
		this.gbc_pnl_kopfzeile.gridx = 0;
		this.gbc_pnl_kopfzeile.gridy = 0;
		this.add(this.pnl_kopfzeile, this.gbc_pnl_kopfzeile);
		// Neue ScrollPane für die JTable
		this.scrollPane = new JScrollPane();
		this.gbc_scrollPane = new GridBagConstraints();
		this.gbc_scrollPane.fill = GridBagConstraints.BOTH;
		this.gbc_scrollPane.insets = new Insets(0, 0, 10, 0);
		this.gbc_scrollPane.gridx = 0;
		this.gbc_scrollPane.gridy = 1;
		this.add(this.scrollPane, this.gbc_scrollPane);
		// Erzeugen der JTable
		this.table = new JTable(new DefaultTableModel(hauptfenster.getPlaylist().getJTable(), Playlist.SPALTENNAMEN));
		this.table.setSelectionBackground(Color.WHITE);
		this.table.setSelectionForeground(Color.BLACK);
		this.table.setGridColor(Color.WHITE);
		this.table.setFocusable(false);
		this.table.setAutoCreateRowSorter(true);
		this.table.setShowVerticalLines(false);
		this.table.setRowHeight(30);
		this.table.setFillsViewportHeight(true);
		this.table.setForeground(Color.WHITE);
		this.table.setBackground(Color.BLACK);
		this.table.setFont(new Font("Tahoma", Font.PLAIN, 25));
		// Erzeugen der Vote Buttons
		new JButtonColumn(this.table, hauptfenster);
		this.scrollPane.setViewportView(this.table);
		// Knopf zum Hinzufügen von neuen Musikwünschen
		this.btn_MusikwunschHinzufgen = new JButton("Musikwunsch hinzufügen");
		this.btn_MusikwunschHinzufgen.setForeground(Color.BLACK);
		this.btn_MusikwunschHinzufgen.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.btn_MusikwunschHinzufgen.addActionListener(arg0 -> hauptfenster.goTo("pnl_Musikwunsch"));
		this.btn_MusikwunschHinzufgen.setBackground(Color.WHITE);
		this.gbc_btn_MusikwunschHinzufgen = new GridBagConstraints();
		this.gbc_btn_MusikwunschHinzufgen.anchor = GridBagConstraints.NORTH;
		this.gbc_btn_MusikwunschHinzufgen.fill = GridBagConstraints.HORIZONTAL;
		this.gbc_btn_MusikwunschHinzufgen.gridx = 0;
		this.gbc_btn_MusikwunschHinzufgen.gridy = 2;
		this.add(this.btn_MusikwunschHinzufgen, this.gbc_btn_MusikwunschHinzufgen);
	}
	
	// Methode zum Aktualisieren
	public void update() {
		this.pnl_kopfzeile.update();
		// Nur falls es einen neuen Musikwunsch gibt wird die Tabelle aktualisiert
		if (this.hauptfenster.getPlaylist().hasUpdate()) {
			this.table = new JTable(new DefaultTableModel(this.hauptfenster.getPlaylist().getJTable(), Playlist.SPALTENNAMEN));
			this.table.setSelectionBackground(Color.WHITE);
			this.table.setSelectionForeground(Color.BLACK);
			this.table.setGridColor(Color.WHITE);
			this.table.setFocusable(false);
			this.table.setAutoCreateRowSorter(true);
			this.table.setShowVerticalLines(false);
			this.table.setRowHeight(30);
			this.table.setFillsViewportHeight(true);
			this.table.setForeground(Color.WHITE);
			this.table.setBackground(Color.BLACK);
			this.table.setFont(new Font("Tahoma", Font.PLAIN, 25));
			new JButtonColumn(this.table, this.hauptfenster);
			this.scrollPane.setViewportView(this.table);
		}
	}
	
}
