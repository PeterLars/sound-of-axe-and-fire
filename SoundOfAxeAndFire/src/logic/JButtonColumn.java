package logic;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

import view.Hauptfenster;

// Klasse des CellEditors der als ActionListener dient
class ButtonEditor extends DefaultCellEditor {
	
	// Attribute
	private static final long	serialVersionUID	= 1L;
	protected JButton			btn;
	private String				lbl;
	private Boolean				clicked;
	private int					row;
	Hauptfenster				hauptfenster;
	
	// Konstruktor
	public ButtonEditor(JTextField textField, Hauptfenster hauptfenster) {
		super(textField);
		this.btn = new JButton();
		this.hauptfenster = hauptfenster;
		this.btn.addActionListener(e -> ButtonEditor.this.fireEditingStopped());
	}
	
	@Override // Methode, die den "Knopfdruck" abfängt
	public Object getCellEditorValue() {
		if (this.clicked)
			this.hauptfenster.vote(this.row);
		this.clicked = false;
		return new String(this.lbl);
	}
	
	@Override // TODO: Kommentar
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		this.lbl = value == null ? "" : value.toString();
		this.btn.setText(this.lbl);
		this.clicked = true;
		this.row = row;
		return this.btn;
	}
}

// Klasse zum Rendern des Buttons
class ButtonRenderer extends JButton implements TableCellRenderer {
	
	// Version UID
	private static final long serialVersionUID = 1L;
	
	// Konstruktor
	public ButtonRenderer() {
		this.setOpaque(true);
		this.setFont(new Font("Tahoma", Font.PLAIN, 25));
		this.setForeground(Color.WHITE);
		this.setBackground(Color.BLACK);
	}
	
	@Override // Gibt das Objekt zurück
	public Component getTableCellRendererComponent(JTable table, Object objekt, boolean isSelected, boolean hasFocus, int row, int column) {
		this.setText(objekt == null ? "" : objekt.toString());
		return this;
	}
	
}

// Klasse, die Aus einer Reihe Strings eine Reihe Buttons macht
public class JButtonColumn extends JFrame {
	
	// Version UID
	private static final long serialVersionUID = 1L;
	
	// Konstruktor
	public JButtonColumn(JTable table, Hauptfenster hauptfenster) {
		super("Button Column");
		int index = Playlist.SPALTENNAMEN.length - 1;
		table.getColumnModel().getColumn(index).setCellRenderer(new ButtonRenderer());
		table.getColumnModel().getColumn(index).setCellEditor(new ButtonEditor(new JTextField(), hauptfenster));
	}
}