package logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import data.Musikwunsch;
import data.MySQLDatabase;

// Klasse zum verwalten der Musikwünsche
public class Playlist extends ArrayList<Musikwunsch> {
	
	// Attribute der Playlist
	private static final long		serialVersionUID	= 1L;
	public static final String[]	SPALTENNAMEN		= {"TitelName", "BandName", "Genre", "Votes"};
	private MySQLDatabase			mySQLDatabase;
	
	// Statische Methode, die eine nach Votes sortierte Liste zurück gibt
	public static Playlist getSortedPlaylist() {
		Playlist sortedPlaylist = new Playlist();
		for (int i = 0; i < sortedPlaylist.size(); i++)
			if (sortedPlaylist.get(i).getVotes() <= 0) {
				sortedPlaylist.remove(i);
				i--;
			}
		Collections.sort(sortedPlaylist);
		return sortedPlaylist;
	}
	
	// Konstruktor
	public Playlist() {
		super();
		this.mySQLDatabase = new MySQLDatabase();
		this.setPlaylist(this.mySQLDatabase.getPlaylist());
	}
	
	// Methode zum Hinzufügen eines Gastes
	public void addGast(String name) {
		this.mySQLDatabase.addGast(name);
	}
	
	// Methode zum Hinzufügen eines Musikwunsches
	public boolean addMusikwunsch(Musikwunsch musikwunsch) {
		return mySQLDatabase.addMusikwunsch(musikwunsch);
	}
	
	// Methode zum Hinzufügen eines Votes
	public boolean addVote(String name, Musikwunsch musikwunsch) {
		return this.mySQLDatabase.addVote(name, musikwunsch);
	}
	
	// Methode, die den Inhalt der Playlist in geeigneter Form für eine JTable ausgibt
	public Object[][] getJTable() {
		this.update();
		Object[][] tabledata = new Object[this.size()][Playlist.SPALTENNAMEN.length];
		for (int i = 0; i < this.size(); i++) {
			tabledata[i][0] = this.get(i).getTitelName();
			tabledata[i][1] = this.get(i).getBandName();
			tabledata[i][2] = this.get(i).getGenre();
			tabledata[i][3] = "Vote";
		}
		return tabledata;
	}
	
	// Methode, die den Inhalt der Playlist in geeigneter Form für eine JTable ausgibt (Sortiert nach Votes)
	public Object[][] getVotedJTable() {
		this.update();
		Playlist sortedPlaylist = Playlist.getSortedPlaylist();
		Object[][] tabledata = new Object[sortedPlaylist.size()][Playlist.SPALTENNAMEN.length];
		for (int i = 0, j = 0; j < sortedPlaylist.size(); i++, j++) {
			tabledata[i][0] = sortedPlaylist.get(j).getTitelName();
			tabledata[i][1] = sortedPlaylist.get(j).getBandName();
			tabledata[i][2] = sortedPlaylist.get(j).getGenre();
			tabledata[i][3] = sortedPlaylist.get(j).getVotes();
		}
		return tabledata;
	}
	
	// Getter der Database
	public MySQLDatabase getMySQLDatabase() {
		return this.mySQLDatabase;
	}
	
	// Methode die überprüft, ob es einen neuen Musikwunsch gibt 
	public boolean hasUpdate() {
		return !this.equals(new Playlist());
	}
	
	// Setter der Database
	public void setMySQLDatabase(MySQLDatabase mySQLDatabase) {
		this.mySQLDatabase = mySQLDatabase;
	}
	
	// Setter der Playlist
	public void setPlaylist(List<Musikwunsch> playlist) {
		this.clear();
		this.addAll(playlist);
	}
	
	// Methode, die die Playlist aktualisiert
	public void update() {
		this.setPlaylist(this.mySQLDatabase.getPlaylist());
	}
}
